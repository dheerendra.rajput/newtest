<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewPostRepository")
 */
class NewPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $only_text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $full_article;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $only_image;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $only_video;

    /**
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnly_Text(): ?string
    {
        return $this->only_text;
    }

    public function setOnly_Text(?string $only_text): self
    {
        $this->only_text = $only_text;

        return $this;
    }

    public function getFull_Article(): ?string
    {
        return $this->full_article;
    }

    public function setFull_Article(?string $full_article): self
    {
        $this->full_article = $full_article;

        return $this;
    }

    public function getOnly_Image(): ?string
    {
        return $this->only_image;
    }

    public function setOnly_Image(?string $only_image): self
    {
        $this->only_image = $only_image;

        return $this;
    }

    public function getOnly_Video(): ?string
    {
        return $this->only_video;
    }

    public function setOnly_Video(?string $only_video): self
    {
        $this->only_video = $only_video;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
