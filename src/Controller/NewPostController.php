<?php

namespace App\Controller;

use App\Entity\NewPost;
use App\Repository\NewPostRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class NewPostController extends AbstractController
{
    

    /**
     * @Route("/new_post_new", name="new_post_new", methods={"GET","POST"})
     */
    public function new_post_new(NewPostRepository $newPostRepository, ArticleRepository $articleRepository, Request $request): Response
    {
        $NewPostText = $request->request->get('text');
        $NewPostFile = $request->files->get('images');
        $NewPostArticle="";
        $NewPostVideo="";
        $NewPost = new NewPost();
        $NewPost->setOnly_Text($NewPostText);
        $NewPost->setFull_Article($NewPostArticle);
        $NewPost->setOnly_Video($NewPostVideo);
        $NewPost->setTime(new \DateTime());
        $NewPost->setDate(new \DateTime());
        $NewPost->setOnly_Image($NewPostFile);
        $uploads_directory=$this->getParameter('uploads_directory');
        $filename=md5(uniqid()).'.'.$NewPostFile->guessExtension();
        $NewPostFile->move(
            $uploads_directory,
            $filename
        );
        $NewPost->setOnly_Image($filename);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($NewPost);
            $entityManager->flush();

            return $this->redirectToRoute('article_index');
        return $this->render('article/index.html.twig', [
            'NewPosts' => $newPostRepository->findAll(),
            'articles' => $articleRepository->findAll()
        ]);
    }
}
