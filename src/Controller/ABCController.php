<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ABCController extends AbstractController
{
    /**
     * @Route("/a/b/c", name="a_b_c")
     */
    public function index()
    {
        return $this->render('abc/index.html.twig', [
            'controller_name' => 'ABCController',
        ]);
    }
}
