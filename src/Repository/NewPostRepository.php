<?php

namespace App\Repository;

use App\Entity\NewPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewPost[]    findAll()
 * @method NewPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NewPost::class);
    }

    // /**
    //  * @return NewPost[] Returns an array of NewPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewPost
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
